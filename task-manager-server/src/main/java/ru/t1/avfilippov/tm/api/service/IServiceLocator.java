package ru.t1.avfilippov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.service.dto.IProjectDTOService;
import ru.t1.avfilippov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.IUserDTOService;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IPropertyService getPropertyService();

}
