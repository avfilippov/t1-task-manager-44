package ru.t1.avfilippov.tm.api.service.dto;

import ru.t1.avfilippov.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO>{
}
