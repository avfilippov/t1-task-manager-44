package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.api.service.dto.IProjectDTOService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.IUserDTOService;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.exception.AbstractException;
import ru.t1.avfilippov.tm.exception.entity.UserNotFoundException;
import ru.t1.avfilippov.tm.exception.field.AbstractFieldException;
import ru.t1.avfilippov.tm.exception.field.LoginEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.service.dto.ProjectDTOService;
import ru.t1.avfilippov.tm.service.dto.TaskDTOService;
import ru.t1.avfilippov.tm.service.dto.UserDTOService;

import java.util.Objects;

import static ru.t1.avfilippov.tm.constant.TestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserDTOService service = new UserDTOService(connectionService, projectService, taskService, new PropertyService());

    @NotNull
    private static final String LOGIN_TEST = "logintest";

    @NotNull
    private static final String PASS_TEST = "logintest";

    @NotNull
    private static final String PASS_RETEST = "logintest";

    @NotNull
    private static final String NAME = "firstName";

    @NotNull
    private UserDTO userTesting;

    @Before
    public void before() throws AbstractException {
        userTesting = service.create(LOGIN_TEST, PASS_TEST, Role.USUAL);
    }

    @After
    public void after() {
        service.removeByLogin(LOGIN_TEST);
    }

    @Test
    public void create() throws AbstractException {
        service.create(LOGIN, PASSWORD, Role.USUAL);
        @Nullable final UserDTO user = service.findByLogin(LOGIN);
        Assert.assertNotNull(user);
        projectService.add(user.getId(), USER_PROJECT1);
        Assert.assertNotNull(projectService.findOneById(USER_PROJECT1.getUserId(), USER_PROJECT1.getId()));
    }

    @Test
    public void findByLogin() throws LoginEmptyException, UserNotFoundException {
        Assert.assertNotNull(service.findByLogin(LOGIN_TEST));
    }


    @Test
    public void removeByLogin() throws AbstractFieldException {
        service.removeByLogin(LOGIN_TEST);
        Assert.assertThrows(Exception.class, () -> service.findByLogin(LOGIN_TEST));
    }

    @Test
    public void updateUser() throws AbstractFieldException {
        service.updateUser(userTesting.getId(), NAME, "lastName", "middle");
        Assert.assertTrue(Objects.equals(userTesting.getFirstName(), NAME));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(service.isLoginExists(LOGIN_TEST));
    }

}
