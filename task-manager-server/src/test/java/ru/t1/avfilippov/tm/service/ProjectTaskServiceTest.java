package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.ITaskService;
import ru.t1.avfilippov.tm.api.service.dto.IProjectDTOService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.exception.AbstractException;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.service.dto.ProjectDTOService;
import ru.t1.avfilippov.tm.service.dto.ProjectTaskDTOService;
import ru.t1.avfilippov.tm.service.dto.TaskDTOService;

import java.util.Objects;

import static ru.t1.avfilippov.tm.constant.TestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private final ProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(projectService, taskService);

    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException {
        projectService.add(USER1.getId(), USER_PROJECT1);
        projectService.add(USER1.getId(), USER_PROJECT2);

        taskService.add(USER1.getId(), USER_TASK1);
        taskService.add(USER1.getId(), USER_TASK2);
    }

    @After
    public void after() throws UserIdEmptyException {
        projectService.clear(USER1.getId());
        taskService.clear(USER1.getId());
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        projectTaskService.bindTaskToProject(USER1.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        Assert.assertTrue(Objects.equals(USER_TASK1.getProjectId(), USER_PROJECT1.getId()));
    }

    @Test
    public void removeProjectById() throws AbstractException {
        projectTaskService.removeProjectById(USER1.getId(), USER_PROJECT1.getId());
        Assert.assertFalse(projectService.existsById(USER_PROJECT1.getUserId(), USER_PROJECT1.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        Assert.assertNull(USER_TASK1.getProjectId());
    }

}
